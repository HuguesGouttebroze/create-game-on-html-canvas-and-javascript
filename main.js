const canvas = document.querySelector('canvas');
console.log(canvas);
const ctx = canvas.getContext('2d');

canvas.width = innerWidth;
canvas.height = innerHeight; 

class Player {
  constructor(x, y, radius, color) {
    this.x = x;
    this.y = y; 
    this.radius = radius;
    this.color = color;
  }

  draw() {
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    ctx.fillStyle = this.color;
    ctx.fill();
  }
}

class Projectile {
  constructor(x, y, radius, color, velocity) {
    this.x = x;
    this.y = y; 
    this.radius = radius;
    this.color = color;
    this.velocity = velocity;
  }

  draw() {
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    ctx.fillStyle = this.color;
    ctx.fill();
  }

  update() {
    /* this.x += this.velocity.x;
    this.y += this.velocity.y; */
    this.draw();
    this.x = this.x + this.velocity.x;
    this.y = this.y + this.velocity.y;
  } 
}

class Enemy {
  constructor(x, y, radius, color, velocity) {
    this.x = x;
    this.y = y; 
    this.radius = radius;
    this.color = color;
    this.velocity = velocity;
  }

  draw() {
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    ctx.fillStyle = this.color;
    ctx.fill();
  }

  update() {
    /* this.x += this.velocity.x;
    this.y += this.velocity.y; */
    this.draw();
    this.x = this.x + this.velocity.x;
    this.y = this.y + this.velocity.y;
  } 
}

// to center player on canvas ... 
const x = canvas.width / 2;
const y = canvas.height / 2;

const player = new Player(x, y, 30, 'blue');

console.log(player);

// contains each projectiles & enemies we will instencies
const projectiles = [];
const enemies = [];

function spawnEnemies() {
  setInterval(() => {
    const radius = Math.random() * (30 - 4) + 4;

    let x;
    let y;

    if (Math.random() < 0.5) {
      x = Math.random() < 0.5 ? 0 - radius : canvas.width + radius;
      y = Math.random() * canvas.height;
    } else {
      x = Math.random() * canvas.width;
      y = Math.random() < 0.5 ? 0 - radius : canvas.height + radius;
    }
    
    const color = 'green';
    
    const angle = Math.atan2(
      canvas.height / 2 - y,  // represent center from click to canvas center
      canvas.width / 2 - x
    );
    // console.log(angle);
    const velocity = {
      x: Math.cos(angle),
      y: Math.sin(angle)
    }

    enemies.push(new Enemy(x, y, radius, color, velocity));
      console.log(enemies);
  }, 1000);
}

function animate() {
  requestAnimationFrame(animate);
  console.log('animate function go');  
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  player.draw();
/*   projectile.draw();
  projectile.update(); */
  projectiles.forEach((projectile) => {
    projectile.update();
  })

  enemies.forEach(enemy => {
    enemy.update();
  })
}

addEventListener('click', (e) => {
  // console.log(e.clientX, e.clientY); // `event`.clientX & clientY: x & y coord. begin to 0 on top left screen 
  const angle = Math.atan2(
    e.clientY - canvas.height / 2, // represent center from click to canvas center
    e.clientX - canvas.width / 2 
  );
  // console.log(angle);
  const velocity = {
    x: Math.cos(angle),
    y: Math.sin(angle)
  }

  projectiles.push(new Projectile(
    canvas.width / 2, 
    canvas.height / 2, 
    5, // radius
    'red', // color
    velocity
  ));

})

animate();
spawnEnemies();