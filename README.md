# Memo - Game HTML Canvas & JavaScript 
## Create a canvas
## Create the PLAYER
+ we can use the Oriented Object paradigme to implement our player
  - create a `Player` class with a `constructor`

    - try to reference the differents properties of a player, like the position, the color, width, height ...

    - MEMO: a `constructor` is called eitch time we instencie a new version of the player class, when we create a new player, we take & reference all of the properties player...

    - exemple
    ```js
    class Player {
      constructor(x, y, radius, color){      
        this.x = x // x (horizontal) axe coordonne
        this.y = y //  y (vertical) axe coordonne
        this.radius = radius // radius property
        this.color = color // color property
      }
    }
    ```
  + we can now instancies or create our new player version, using `new` word & reference our constructor Player class properties

    ```js
    const player = new Player(10, 20, 12, 'red')
    ```
  + now, we have create our Player instance buy we need to create also a draw on canvas function using our canvas context

  ```js
    class Player {
      constructor(x, y, radius, color){      
        this.x = x // x (horizontal) axe coordonne
        this.y = y //  y (vertical) axe coordonne
        this.radius = radius // radius property
        this.color = color // color property
      }
      draw() {
        ctx.be........
        ctx.arc(....) // (method) CanvasPath.arc(x: number, y: number, radius: number, startAngle: number, endAngle: number, counterclockwise?: boolean | undefined): void
        ctx.fillStyle = this.color;
        ctx.fill()
      }
    }

    player.draw()
    
    ```

    + if we wanna fix player on canvas center

    ```js
      // to center player on canvas ... 
      const x = canvas.width / 2;
      const y = canvas.height / 2;
      const player = new Player(x, y, 30, 'blue');
    ```

## Shoot Projectiles

  + To Shoot Projectiles from our Player location

    + create a Projectile class
      - like a ref., or, a model, that will be used to instenced all our projectiles with differents properties ...
        - same properties player but with a velocity property ...
      - this class describe the properies from our projectiles we will create on futur ...

    + next, we'll create a event listener to create a new projectile on each click on canvas

    ```js
      addEventListener('click', (e) => {
        console.log(e.clientX); // `event`.clientX & clientY: x & y coord. begin to 0 on top left screen
        const projectile = new Projectile(
          e.clientX, e.clientY, 5, 'red', null
        );
        projectile.draw();
      })
    ```

    + Create a `animate()` function to call the `requestAnimationFrame()` method, that called this `animate()` fnc as callback function

    + Create a `group of projectiles`
      - Create an empty array of projectiles & push new projectiles into this array

      - & foreach projectile of projectiles group (array):
        - we want to call the `update()` function, as:

      ```js
      projectiles.forEach((projectile) => {
        projectile.update();
      })
      ```
      + we must also called our `draw()` function inside our `update()` function, from `Projectile` class, as: 

      ```js
        update() {
          this.draw();
          this.x = this.x + this.velocity.x;
          this.y = this.y + this.velocity.y;
        } 
      ```
      + we could generate our projectile, as a static way, like this:

      ```js
      const projectile = new Projectile(
        canvas.width / 2,
        canvas.height / 2, 
        // e.clientX,
        // e.clientY,
        5, // radius
        'red', // color
        { x: 1, y: 1 } // velocity
      ); 

      const projectile2 = new Projectile(
          canvas.width / 2,
          canvas.height / 2, 
          // e.clientX,
          // e.clientY,
          5, // radius
          'orange', // color
          { x: -1, y: -1 } // velocity
        ); 

      const projectiles = [projectile, projectile2];
      ```

      + but we rather make it as a dynamical way, into an `event listener` on click, and push ever new projectile properties into the center of the screen 
      (properties are `x` & `y` axe, `radius`, `color` & `velocity` values):

      ```js
        addEventListener('click', (e) => {
          projectiles.push(new Projectile(
            canvas.width / 2, 
            canvas.height / 2, 
            5, // radius
            'red', // color
            null // velocity
          ));
        })
      ```

# Source, Doc.
+ See [requestAnimationFrame() MDN documentation](https://developer.mozilla.org/fr/docs/Web/API/Window/requestAnimationFrame).
